package ru.rummy.cropper.presenters;

import android.graphics.Bitmap;

import org.opencv.core.Mat;

import ru.rummy.cropper.views.IMainView;

public interface IMainPresenter {
    void onFaceSizeChanged();

    void onCropSizeChanged();

    void onLoadOriginalImage();

    void onSaveCroppedImage();

    void onSaveSaliencyImage();

    void onSaveBinaryImage();

    Bitmap prepareToViewImage(Mat image);

    Mat prepareToInteractor(Bitmap image);

    void onBind(IMainView view);

    void onUnbind();
}
