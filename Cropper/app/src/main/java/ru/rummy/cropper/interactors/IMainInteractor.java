package ru.rummy.cropper.interactors;

import org.opencv.core.Mat;

public interface IMainInteractor {
    Mat getImage(String path);

    Mat evaluateSaliencyImage();

    Mat evaluateBinaryImage();

    Mat evaluateCroppedImage();

    Mat getCroppedImage();

    Mat getSaliencyImage();

    Mat getBinaryImage();

    void saveImage(String path);
}
