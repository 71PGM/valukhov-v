package ru.rummy.cropper.presenters;

import android.graphics.Bitmap;

import org.opencv.core.Mat;

import ru.rummy.cropper.views.IMainView;

public class MainPresenter implements IMainPresenter{
    @Override
    public void onFaceSizeChanged() {

    }

    @Override
    public void onCropSizeChanged() {

    }

    @Override
    public void onLoadOriginalImage() {

    }

    @Override
    public void onSaveCroppedImage() {

    }

    @Override
    public void onSaveSaliencyImage() {

    }

    @Override
    public void onSaveBinaryImage() {

    }

    @Override
    public Bitmap prepareToViewImage(Mat image) {
        return null;
    }

    @Override
    public Mat prepareToInteractor(Bitmap image) {
        return null;
    }

    @Override
    public void onBind(IMainView view) {

    }

    @Override
    public void onUnbind() {

    }
}
