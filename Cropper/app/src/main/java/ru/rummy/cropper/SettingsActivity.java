package ru.rummy.cropper;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.RadioGroup;

public class SettingsActivity extends AppCompatActivity {
    private RadioGroup radioGroup;
    private SharedPreferences prefSaliency;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        radioGroup = findViewById(R.id.radio_group);
        prefSaliency = getApplicationContext().getSharedPreferences("id_saliency_detector", MODE_PRIVATE);

        int id = prefSaliency.getInt("id", 0);
        switch (id) {
            case 0:
                radioGroup.check(R.id.sr_radio);
                break;
            case 1:
                radioGroup.check(R.id.lgss_radio);
                break;
        }

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.sr_radio:
                        prefSaliency.edit().putInt("id", 0).apply();
                        break;
                    case R.id.lgss_radio:
                        prefSaliency.edit().putInt("id", 1).apply();
                        break;
                }
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
