package ru.rummy.cropper.interactors;

import org.opencv.core.Mat;

public class MainInteractor implements IMainInteractor {
    @Override
    public Mat getImage(String path) {
        return null;
    }

    @Override
    public Mat evaluateSaliencyImage() {
        return null;
    }

    @Override
    public Mat evaluateBinaryImage() {
        return null;
    }

    @Override
    public Mat evaluateCroppedImage() {
        return null;
    }

    @Override
    public Mat getCroppedImage() {
        return null;
    }

    @Override
    public Mat getSaliencyImage() {
        return null;
    }

    @Override
    public Mat getBinaryImage() {
        return null;
    }

    @Override
    public void saveImage(String path) {

    }
}
