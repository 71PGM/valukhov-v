package ru.rummy.cropper;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import ru.rummy.cropper.views.IMainView;

public class NewMainActivity extends AppCompatActivity implements IMainView {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setOriginalImage(Bitmap image) {

    }

    @Override
    public void setCroppedImage(Bitmap image) {

    }

    @Override
    public void setSaliencyImage(Bitmap image) {

    }

    @Override
    public void setBinaryImage(Bitmap image) {

    }
}
