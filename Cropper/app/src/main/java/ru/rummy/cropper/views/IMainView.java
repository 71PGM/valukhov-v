package ru.rummy.cropper.views;

import android.graphics.Bitmap;

public interface IMainView {
    void setOriginalImage(Bitmap image);

    void setCroppedImage(Bitmap image);

    void setSaliencyImage(Bitmap image);

    void setBinaryImage(Bitmap image);
}
