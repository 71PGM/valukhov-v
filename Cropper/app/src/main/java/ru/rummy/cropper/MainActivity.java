package ru.rummy.cropper;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import static org.opencv.core.Core.BORDER_DEFAULT;
import static org.opencv.core.Core.DFT_INVERSE;
import static org.opencv.core.Core.hconcat;
import static org.opencv.core.Core.multiply;
import static org.opencv.core.Core.normalize;
import static org.opencv.core.Core.sumElems;
import static org.opencv.core.Core.vconcat;
import static org.opencv.core.CvType.CV_32F;
import static org.opencv.core.CvType.CV_32FC1;
import static org.opencv.core.CvType.CV_64F;
import static org.opencv.core.CvType.CV_64FC1;
import static org.opencv.core.CvType.CV_8UC1;
import static org.opencv.core.CvType.CV_8UC4;
import static org.opencv.imgproc.Imgproc.CHAIN_APPROX_SIMPLE;
import static org.opencv.imgproc.Imgproc.COLOR_BGR2GRAY;
import static org.opencv.imgproc.Imgproc.COLOR_BGR2Lab;
import static org.opencv.imgproc.Imgproc.COLOR_GRAY2BGRA;
import static org.opencv.imgproc.Imgproc.GaussianBlur;
import static org.opencv.imgproc.Imgproc.INTER_LINEAR_EXACT;
import static org.opencv.imgproc.Imgproc.MORPH_CLOSE;
import static org.opencv.imgproc.Imgproc.MORPH_ELLIPSE;
import static org.opencv.imgproc.Imgproc.MORPH_OPEN;
import static org.opencv.imgproc.Imgproc.RETR_LIST;
import static org.opencv.imgproc.Imgproc.THRESH_BINARY;
import static org.opencv.imgproc.Imgproc.THRESH_OTSU;
import static org.opencv.imgproc.Imgproc.blur;
import static org.opencv.imgproc.Imgproc.boundingRect;
import static org.opencv.imgproc.Imgproc.cvtColor;
import static org.opencv.imgproc.Imgproc.findContours;
import static org.opencv.imgproc.Imgproc.getStructuringElement;
import static org.opencv.imgproc.Imgproc.integral;
import static org.opencv.imgproc.Imgproc.morphologyEx;
import static org.opencv.imgproc.Imgproc.resize;
import static org.opencv.imgproc.Imgproc.threshold;

public class MainActivity extends AppCompatActivity {
    public static final String TAG = "MainActivity";

    private SharedPreferences prefSaliency;

    private String fileName = "";

    public static final int PATCH_WIDTH = 11;
    public static final int PATCH_HEIGHT = 11;
    public static final int GALLERY_REQUEST = 123;
    public static final int slidingStep = 2;
    public static final String[] cropSizeNames = {"Адаптивная", "128x128", "256x256", "512x512", "768x768", "1024x1024"};
    public static final int CROP_SIZE_ADAPTIVE = 0;
    public static final int CROP_SIZE_128 = 128;
    public static final int CROP_SIZE_256 = 256;
    public static final int CROP_SIZE_512 = 512;
    public static final int CROP_SIZE_768 = 768;
    public static final int CROP_SIZE_1024 = 1024;
    public static final int[] cropSizeValues = {CROP_SIZE_ADAPTIVE, CROP_SIZE_128, CROP_SIZE_256, CROP_SIZE_512, CROP_SIZE_768, CROP_SIZE_1024};
    public static final String[] relativeFaceSizesNames = {"0.05", "0.1", "0.15", "0.2", "0.5"};
    public static final float[] relativeFaceSizesValues = {0.05f, 0.1f, 0.15f, 0.2f, 0.5f};
    private static final Scalar FACE_RECT_COLOR = new Scalar(255, 255, 255, 255);
    ImageButton loadBtn;
    //    Button processBtn;
    ImageButton saveBtn;
    ImageButton faceSizeBtn;
    ImageButton cropSizeBtn;
    Button showBackdrop;
    ImageButton saveSaliencyBtn;
    ImageButton saveBinaryBtn;
    ImageView beforeProcessImg;
    ImageView afterProcessImg;
    ImageView binaryImg;
    ImageView thumbnailImg;
    Bitmap beforeProcessBit;
    Bitmap afterProcessBit;
    Bitmap binaryBit;
    Bitmap thumbnailBit;
    BottomSheetBehavior mBottomSheetBehavior;
    private File mCascadeFile;
    private CascadeClassifier mJavaDetector;
    private int scaleCoeff = 1;
    private int cropSizeValue = 1;
    private float mRelativeFaceSize = 0.05f;
    private int relativeFaceSizeInd = 0;
    private int relativeFaceSizeIndTmp = 0;
    private int mAbsoluteFaceSize = 0;
    private int currentCropSize = 0;

    public static Mat Saliency(Mat img) {
        // blur image with a 3x3 or 5x5 Gaussian filter
        Mat gfbgr = new Mat();
        Imgproc.GaussianBlur(img, gfbgr, new Size(3, 3), 3);
        // Perform sRGB to CIE Lab color space conversion
        Mat LabIm = new Mat();
        cvtColor(gfbgr, LabIm, Imgproc.COLOR_BGR2Lab);
//        cvtColor(gfbgr, LabIm, Imgproc.COLOR_BGR2RGB);
        // Compute Lab average values (note that in the paper this average is found from the
        // un-blurred original image, but the results are quite similar)
        List<Mat> lab = new ArrayList<>();
        Core.split(LabIm, lab);
        Mat l = lab.get(0);
        l.convertTo(l, CvType.CV_32FC1);
        Mat a = lab.get(1);
        a.convertTo(a, CvType.CV_32FC1);
        Mat b = lab.get(2);
        b.convertTo(b, CvType.CV_32FC1);
        double lm = Core.mean(l).val[0];
        double am = Core.mean(a).val[0];
        double bm = Core.mean(b).val[0];
        // Finally compute the saliency map
        Mat sm = Mat.zeros(l.rows(), l.cols(), l.type());
        Core.subtract(l, new Scalar(lm), l);
        Core.subtract(a, new Scalar(am), a);
        Core.subtract(b, new Scalar(bm), b);
        Core.add(sm, l.mul(l), sm);
        Core.add(sm, a.mul(a), sm);
        Core.add(sm, b.mul(b), sm);
        return sm;
    }

    public Mat SingleScaleLocalGlobalSaliency(Mat inputImg) {

        Size resizedImageSize = new Size(177, 177);
        Mat inputImgResized = new Mat();
        resize(inputImg, inputImgResized, resizedImageSize, 0, 0, INTER_LINEAR_EXACT);

        int expandX = 0;
        int expandY = 0;

        int patchesInRow = 0;
        int patchesInCol = 0;

        /////////////////////////////
        // EXPAND IMAGE
        /////////////////////////////
        List<Mat> expandedList = new ArrayList<>();
        expandedList.add(inputImgResized);
//        Mat expandCols;
//        Mat expandRows;

        patchesInRow = inputImgResized.cols() / PATCH_WIDTH;
//        if (inputImg.cols() % PATCH_WIDTH != 0) {
//            expandX = PATCH_WIDTH - inputImg.cols() % PATCH_WIDTH;
//            expandedList.add(new Mat(inputImg.rows(), expandX, inputImg.type()));
//            Mat tmp = new Mat();
//            hconcat(expandedList, tmp);
//            expandedList.clear();
//            expandedList.add(tmp);
//            patchesInRow++;
//        }

        patchesInCol = inputImgResized.rows() / PATCH_HEIGHT;
//        if (inputImg.rows() % PATCH_HEIGHT != 0) {
//            expandY = PATCH_HEIGHT - inputImg.rows() % PATCH_HEIGHT;
//            expandedList.add(new Mat(expandY, inputImg.cols() + expandX, inputImg.type()));
//            Mat tmp = new Mat();
//            vconcat(expandedList, tmp);
//            expandedList.clear();
//            expandedList.add(tmp);
//            patchesInCol++;
//        }

        /////////////////////////////
        // TO CIE LAB CONVERTION
        /////////////////////////////
        Mat expandedImage = expandedList.get(0);
        Mat labOriginal = new Mat();
        cvtColor(expandedImage, labOriginal, COLOR_BGR2Lab);

        /////////////////////////////
        // SPLITTING CHANNELS
        /////////////////////////////
        List<Mat> labSplitted = new ArrayList<>();
        Core.split(labOriginal, labSplitted);
        Mat lLab = labSplitted.get(0);
        lLab.convertTo(lLab, CvType.CV_64FC1, 1.0d/255.0d);
//        normalize(lLab, lLab, 0.0d, 1.0d);
        Mat aLab = labSplitted.get(1);
        aLab.convertTo(aLab, CvType.CV_64FC1, 1.0d/255.0d);
//        normalize(aLab, aLab, 0.0d, 1.0d);
        Mat bLab = labSplitted.get(2);
        bLab.convertTo(bLab, CvType.CV_64FC1, 1.0d/255.0d);
//        normalize(bLab, bLab, 0.0d, 1.0d);

        /////////////////////////////
        // INTEGRAL IMAGES
        /////////////////////////////
        Mat lLabIntegral = new Mat();
        Mat aLabIntegral = new Mat();
        Mat bLabIntegral = new Mat();
        integral(lLab, lLabIntegral);
        lLab.row(0).setTo(new Scalar(1));
        lLab.col(0).setTo(new Scalar(1));
        integral(aLab, aLabIntegral);
        aLab.row(0).setTo(new Scalar(1));
        aLab.col(0).setTo(new Scalar(1));
        integral(bLab, bLabIntegral);
        bLab.row(0).setTo(new Scalar(1));
        bLab.col(0).setTo(new Scalar(1));

        List<Mat> rowOfPatches = new ArrayList<>();
        List<Mat> colOfPatches = new ArrayList<>();
        HashMap<String, Double> dcHash = new HashMap<>();

        for (int i = 0; i < patchesInCol; i++) {
            for (int j = 0; j < patchesInRow; j++) {

                int x = j * PATCH_WIDTH + 1;
                int y = i * PATCH_HEIGHT + 1;
                int centerX = x + (PATCH_WIDTH / 2 + 1);
                int centerY = y + (PATCH_HEIGHT / 2 + 1);

                double saliencyOfRegion = 0;
                for (int p = 0; p < patchesInCol; p++) {
                    for (int q = 0; q < patchesInRow; q++) {
                        int x2 = q * PATCH_WIDTH + 1;
                        int y2 = p * PATCH_HEIGHT + 1;
                        int centerX2 = x2 + (PATCH_WIDTH / 2 + 1);
                        int centerY2 = y2 + (PATCH_HEIGHT / 2 + 1);

                        double dp = Math.sqrt(Math.pow(centerX - centerX2, 2) + Math.pow(centerY - centerY2, 2)) / Math.max(inputImgResized.cols(), inputImgResized.rows());
                        double dc = 0;

                        if (dcHash.containsKey(String.valueOf(p) + String.valueOf(q) + "_" + String.valueOf(i) + String.valueOf(j)))  {
                            dc = dcHash.get(String.valueOf(p) + String.valueOf(q) + "_" + String.valueOf(i) + String.valueOf(j));
                        } else {
                            double fli = getMeanValueOfPixels(lLabIntegral, x, y, x + PATCH_WIDTH, y + PATCH_HEIGHT);
                            double flj = getMeanValueOfPixels(lLabIntegral, x2, y2, x2 + PATCH_WIDTH, y2 + PATCH_HEIGHT);
                            double fai = getMeanValueOfPixels(aLabIntegral, x, y, x + PATCH_WIDTH, y + PATCH_HEIGHT);
                            double faj = getMeanValueOfPixels(aLabIntegral, x2, y2, x2 + PATCH_WIDTH, y2 + PATCH_HEIGHT);
                            double fbi = getMeanValueOfPixels(bLabIntegral, x, y, x + PATCH_WIDTH, y + PATCH_HEIGHT);
                            double fbj = getMeanValueOfPixels(bLabIntegral, x2, y2, x2 + PATCH_WIDTH, y2 + PATCH_HEIGHT);
                            dc = Math.sqrt(
                                    Math.pow(fli - flj, 2)
                                            + Math.pow(fai - faj, 2)
                                            + Math.pow(fbi - fbj, 2)
                            );
                            dcHash.put(String.valueOf(i) + String.valueOf(j) + "_" + String.valueOf(p) + String.valueOf(q), dc);
                        }
//                        double fli = getMeanValueOfPixels(lLabIntegral, x, y, x + PATCH_WIDTH, y + PATCH_HEIGHT);
//                        double flj = getMeanValueOfPixels(lLabIntegral, x2, y2, x2 + PATCH_WIDTH, y2 + PATCH_HEIGHT);
//                        double fai = getMeanValueOfPixels(aLabIntegral, x, y, x + PATCH_WIDTH, y + PATCH_HEIGHT);
//                        double faj = getMeanValueOfPixels(aLabIntegral, x2, y2, x2 + PATCH_WIDTH, y2 + PATCH_HEIGHT);
//                        double fbi = getMeanValueOfPixels(bLabIntegral, x, y, x + PATCH_WIDTH, y + PATCH_HEIGHT);
//                        double fbj = getMeanValueOfPixels(bLabIntegral, x2, y2, x2 + PATCH_WIDTH, y2 + PATCH_HEIGHT);
//                        double dc = Math.sqrt(
//                                Math.pow(fli - flj, 2)
//                                + Math.pow(fai - faj, 2)
//                                + Math.pow(fbi - fbj, 2)
//                        );

                        saliencyOfRegion = saliencyOfRegion + (1 / (1 + 3 * dp ) * dc);
                    }
                }

                saliencyOfRegion = 3*(1 - Math.exp(- 1.0d / (patchesInCol * patchesInRow) * saliencyOfRegion));
                Mat tmp = Mat.ones(new Size(PATCH_WIDTH, PATCH_HEIGHT), CV_64FC1);
                multiply(tmp, new Scalar(saliencyOfRegion), tmp);
                rowOfPatches.add(tmp);

            }
            Mat tmp = new Mat();
            hconcat(rowOfPatches, tmp);
            colOfPatches.add(tmp);
            rowOfPatches.clear();
        }
        Mat tmp = new Mat();
        vconcat(colOfPatches, tmp);
        Mat convertedTmp = new Mat();
        tmp.convertTo(convertedTmp, CV_32FC1);

        //Mat saliencyMap = new Mat(convertedTmp, new Rect( new Point(expandX, expandY), inputImg.size()));
        Mat saliencyMap = new Mat();
        resize(tmp, saliencyMap, inputImg.size(), 0, 0, INTER_LINEAR_EXACT);
        //Mat saliencyMap = new Mat(convertedTmp, new Rect( new Point(0, 0), new Point(patchesInRow*PATCH_WIDTH, patchesInCol*PATCH_HEIGHT)));

        return saliencyMap;
    }

    public double getMeanValueOfPixels(Mat integralImage, int x1, int y1, int x2, int y2) {
        return (integralImage.get(y2, x2)[0] - integralImage.get(y1 - 1, x2)[0] - integralImage.get(y2, x1 - 1)[0] + integralImage.get(y1 - 1, x1 - 1)[0]) / (PATCH_HEIGHT * PATCH_WIDTH);
    }

    public static Mat StaticSaliencySpectralResidual(Mat inputImg) {
        Mat imageGR = new Mat();
        cvtColor(inputImg, imageGR, COLOR_BGR2GRAY);

        Mat grayDown = new Mat();
        Size resizedImageSize = new Size(256, 256);
        resize(imageGR, grayDown, resizedImageSize, 0, 0, INTER_LINEAR_EXACT);

        Mat realImage = new Mat(resizedImageSize, CV_64F);
        grayDown.convertTo(realImage, CV_64F);

        Mat imaginaryImage = new Mat(resizedImageSize, CV_64F);
        Mat combinedImage = new Mat(resizedImageSize, CV_64F);
        Mat imageDFT = new Mat();

        List<Mat> mv = new ArrayList<>();
        mv.add(imaginaryImage);
        mv.add(realImage);
        Core.merge(mv, combinedImage);
        Core.dft(combinedImage, imageDFT);
        Core.split(imageDFT, mv);

        Mat magnitude = new Mat(resizedImageSize, CV_64F);
        Mat magnitude_tmp = new Mat(resizedImageSize, CV_64F);
        Mat angle = new Mat(resizedImageSize, CV_64F);
        Mat logAmplitude = new Mat();
        Mat logAmplitude_blur = new Mat();
        Mat logAmplitude_tmp = new Mat();

        Core.cartToPolar(mv.get(0), mv.get(1), magnitude, angle, false);
        Core.add(magnitude, new Scalar(1), magnitude_tmp);
        //TODO: check
        Core.log(magnitude_tmp, logAmplitude);
        blur(logAmplitude, logAmplitude_blur, new Size(3, 3), new Point(-1, -1), BORDER_DEFAULT);

        Core.subtract(logAmplitude, logAmplitude_blur, logAmplitude_tmp);
        Core.exp(logAmplitude_tmp, magnitude);
        Core.polarToCart(magnitude, angle, mv.get(0), mv.get(1));
        Core.merge(mv, imageDFT);
        Core.dft(imageDFT, combinedImage, DFT_INVERSE);
        Core.split(combinedImage, mv);

        Core.cartToPolar(mv.get(0), mv.get(1), magnitude, angle, false);
        GaussianBlur(magnitude, magnitude, new Size(5, 5), 8, 0, BORDER_DEFAULT);
        magnitude = magnitude.mul(magnitude);

        Core.MinMaxLocResult minMaxLocResult = new Core.MinMaxLocResult();
        minMaxLocResult = Core.minMaxLoc(magnitude);

        Mat magnitude_tmp_tmp = new Mat(resizedImageSize, CV_64F);
        Core.divide(magnitude, new Scalar(minMaxLocResult.maxVal), magnitude_tmp_tmp);
        Mat magnitude_tmp_tmp_tmp = new Mat(resizedImageSize, CV_64F);
        magnitude_tmp_tmp.convertTo(magnitude_tmp_tmp_tmp, CV_32F);

        Mat saliencyMap = new Mat();
        resize(magnitude_tmp_tmp_tmp, saliencyMap, inputImg.size(), 0, 0, INTER_LINEAR_EXACT);

        return saliencyMap;
    }

    public static Mat StaticSaliencyFineGrained(Mat inputImg) {
        //
        return null;
    }

    public static void sortContoursByArea(List<MatOfPoint> contours) {
        Collections.sort(contours, new Comparator<MatOfPoint>() {

            @Override
            public int compare(MatOfPoint a, MatOfPoint b) {
                // Not sure how expensive this will be computationally as the
                // area is computed for each comparison
                double areaA = Imgproc.contourArea(a);
                double areaB = Imgproc.contourArea(b);

                // Change sign depending on whether your want sorted small to big
                // or big to small
                if (areaA < areaB) {
                    return 1;
                } else if (areaA > areaB) {
                    return -1;
                }
                return 0;
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        prefSaliency = getApplicationContext().getSharedPreferences("id_saliency_detector", MODE_PRIVATE);

        loadBtn = findViewById(R.id.load_image);
//        processBtn = findViewById(R.id.process_image);
        saveBtn = findViewById(R.id.save_thumbnail);
        faceSizeBtn = findViewById(R.id.change_face_size);
        cropSizeBtn = findViewById(R.id.change_crop_size);

        showBackdrop = findViewById(R.id.show_backdrop);

        saveSaliencyBtn = findViewById(R.id.save_saliency);
        saveBinaryBtn = findViewById(R.id.save_binary);

        beforeProcessImg = findViewById(R.id.original_image);
        afterProcessImg = findViewById(R.id.saliency_map);
        binaryImg = findViewById(R.id.binary_map);
        thumbnailImg = findViewById(R.id.thumbnail_image);

        loadBtn.setOnClickListener(loadImage());
//        processBtn.setOnClickListener(processImage());
        saveBtn.setOnClickListener(saveImage(getApplicationContext()));
        faceSizeBtn.setOnClickListener(changeFaceSize());
        cropSizeBtn.setOnClickListener(changeCropSize());

        try {
            // load cascade file from application resources
            InputStream is = getResources().openRawResource(R.raw.lbpcascade_frontalface);
            File cascadeDir = getDir("cascade", Context.MODE_PRIVATE);
            mCascadeFile = new File(cascadeDir, "lbpcascade_frontalface.xml");
            FileOutputStream os = new FileOutputStream(mCascadeFile);

            byte[] buffer = new byte[4096];
            int bytesRead;
            while ((bytesRead = is.read(buffer)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
            is.close();
            os.close();

            mJavaDetector = new CascadeClassifier(mCascadeFile.getAbsolutePath());
            if (mJavaDetector.empty()) {
                Log.e(TAG, "Failed to load cascade classifier");
                mJavaDetector = null;
            } else
                Log.i(TAG, "Loaded cascade classifier from " + mCascadeFile.getAbsolutePath());

            cascadeDir.delete();

        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, "Failed to load cascade. Exception thrown: " + e);
        }

        configureBackdrop();
    }

    public void saveImageMethod(final Context context, final Bitmap bitmap, final String fileNamePrefix) {
        if (bitmap == null) return;

        PermissionListener permissionlistener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
//                        Toast.makeText(MainActivity.this, "Permission Granted", Toast.LENGTH_SHORT).show();
                String uuid = UUID.randomUUID().toString();
                String filename = fileNamePrefix.concat("_").concat(uuid).concat(".png");

                File sd = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + File.separator + "Cropper/");
                boolean success = true;
                if (!sd.exists()) {
                    success = sd.mkdirs();
                }
                if (success) {
                    File dest = new File(sd, filename);

                    try {
                        FileOutputStream out = new FileOutputStream(dest);
                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
                        out.close();
                        Toast.makeText(MainActivity.this, "Сохранено: ".concat(dest.getPath()), Toast.LENGTH_SHORT).show();

                        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                        File f = new File(dest.getAbsolutePath());
                        Uri contentUri = Uri.fromFile(f);
                        mediaScanIntent.setData(contentUri);
                        context.sendBroadcast(mediaScanIntent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onPermissionDenied(List<String> deniedPermissions) {
                Toast.makeText(MainActivity.this, "Нет необходимых разрешений", Toast.LENGTH_SHORT).show();
            }


        };

        TedPermission.with(context)
                .setPermissionListener(permissionlistener)
                .setDeniedMessage("Если Вы откажете в выдаче требуемых разрешений, то не сможете воспользоваться функцией выгрузки")
                .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .check();
    }

    public View.OnClickListener loadImage() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, GALLERY_REQUEST);
            }
        };
    }

    public View.OnClickListener saveImage(final Context context) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (thumbnailBit == null) return;

                PermissionListener permissionlistener = new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
//                        Toast.makeText(MainActivity.this, "Permission Granted", Toast.LENGTH_SHORT).show();
                        String uuid = UUID.randomUUID().toString();
                        String filename = "Thumbnail".concat("_").concat(uuid).concat(".png");

                        File sd = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + File.separator + "Cropper/");
                        boolean success = true;
                        if (!sd.exists()) {
                            success = sd.mkdirs();
                        }
                        if (success) {
                            File dest = new File(sd, filename);

                            try {
                                FileOutputStream out = new FileOutputStream(dest);
                                thumbnailBit.compress(Bitmap.CompressFormat.PNG, 100, out);
                                out.close();
                                Toast.makeText(MainActivity.this, "Сохранено: ".concat(dest.getPath()), Toast.LENGTH_SHORT).show();

                                Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                                File f = new File(dest.getAbsolutePath());
                                Uri contentUri = Uri.fromFile(f);
                                mediaScanIntent.setData(contentUri);
                                context.sendBroadcast(mediaScanIntent);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onPermissionDenied(List<String> deniedPermissions) {
                        Toast.makeText(MainActivity.this, "Нет необходимых разрешений", Toast.LENGTH_SHORT).show();
                    }


                };

                TedPermission.with(context)
                        .setPermissionListener(permissionlistener)
                        .setDeniedMessage("Если Вы откажете в выдаче требуемых разрешений, то не сможете воспользоваться функцией выгрузки")
                        .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .check();

//                try (FileOutputStream out = context.openFileOutput("Thumbnail_".concat(uuid), Context.MODE_PRIVATE)) {
//                    thumbnailBit.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
//                    // PNG is a lossless format, the compression factor (100) is ignored
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
            }
        };
    }

    public View.OnClickListener changeCropSize() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Размер кадрирующей рамки")
//                        .setMessage("Выберите размер кадрирующей рамки")
                        .setCancelable(false)
                        .setPositiveButton("Принять", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int i) {
//                                Toast.makeText(MainActivity.this, "Выбран ".concat(String.valueOf(currentCropSize)), Toast.LENGTH_SHORT).show();
                                evaluateCropSize();
                                processImage().onClick(cropSizeBtn);
                                dialog.dismiss();
                            }
                        })
                        .setNegativeButton("Отменить",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                })
                        .setSingleChoiceItems(cropSizeNames, currentCropSize, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int item) {
                                currentCropSize = item;
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        };
    }

    public View.OnClickListener changeFaceSize() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Относительный размер лица")
//                        .setMessage("Выберите размер кадрирующей рамки")
                        .setCancelable(false)
                        .setPositiveButton("Принять", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int i) {
                                relativeFaceSizeInd = relativeFaceSizeIndTmp;
                                mRelativeFaceSize = relativeFaceSizesValues[relativeFaceSizeInd];
                                processImage().onClick(cropSizeBtn);
                                dialog.dismiss();
                            }
                        })
                        .setNegativeButton("Отменить",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                })
                        .setSingleChoiceItems(relativeFaceSizesNames, relativeFaceSizeInd, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int item) {
                                relativeFaceSizeIndTmp = item;
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        };
    }

    public View.OnClickListener processImage() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (beforeProcessBit == null) return;

                /////////////////////////////
                // INPUT AND SALIENCY DETECTOR
                /////////////////////////////

                Mat in = new Mat();
                Utils.bitmapToMat(beforeProcessBit, in);

                int idDetector = prefSaliency.getInt("id", 0);
                Mat out = new Mat();
                switch (idDetector) {
                    case 0:
                        out = StaticSaliencySpectralResidual(in);
                        break;
                    case 1:
                        out = SingleScaleLocalGlobalSaliency(in);
                        break;
                }

                //Mat out = StaticSaliencySpectralResidual(in);
                //Mat out = SingleScaleLocalGlobalSaliency(in);

                Mat in1 = new Mat();

                Mat out1 = new Mat();
                Mat out2 = new Mat();
                out.convertTo(out1, CV_8UC1);
                out.convertTo(out2, CV_8UC1, 255, 0);

                /////////////////////////////
                // FILTER
                /////////////////////////////

                Mat blur = new Mat();
                Imgproc.GaussianBlur(out2, blur, new Size(9, 9), 3);

                Mat binaryMap = new Mat(blur.size(), blur.type());
                threshold(blur, binaryMap, 0, 255, THRESH_BINARY | THRESH_OTSU);

                Mat opening = new Mat(binaryMap.size(), binaryMap.type());
                Mat closing = new Mat(binaryMap.size(), binaryMap.type());
                Mat closing1 = new Mat();

                switch (idDetector) {
                    case 0:
                        Mat kernel = getStructuringElement(MORPH_ELLIPSE, new Size(10, 10));
                        opening = new Mat(binaryMap.size(), binaryMap.type());
                        morphologyEx(binaryMap, opening, MORPH_OPEN, kernel, new Point(kernel.size().height / 2, kernel.size().width / 2), 1);
                        closing = new Mat(opening.size(), opening.type());
                        kernel = getStructuringElement(MORPH_ELLIPSE, new Size(16, 16));
                        morphologyEx(opening, closing, MORPH_CLOSE, kernel, new Point(kernel.size().height / 2, kernel.size().width / 2), 3);
                        break;
                    case 1:
                        Mat kernel2 = getStructuringElement(MORPH_ELLIPSE, new Size(10, 10));
                        opening = new Mat(binaryMap.size(), binaryMap.type());
                        morphologyEx(binaryMap, opening, MORPH_OPEN, kernel2, new Point(kernel2.size().height / 2, kernel2.size().width / 2), 1);
                        closing = new Mat(opening.size(), opening.type());
                        kernel2 = getStructuringElement(MORPH_ELLIPSE, new Size(16, 16));
                        morphologyEx(opening, closing, MORPH_CLOSE, kernel2, new Point(kernel2.size().height / 2, kernel2.size().width / 2), 3);
                        break;
                }

//                Mat kernel = getStructuringElement(MORPH_ELLIPSE, new Size(10, 10));
//                Mat opening = new Mat(binaryMap.size(), binaryMap.type());
//                morphologyEx(binaryMap, opening, MORPH_OPEN, kernel, new Point(kernel.size().height / 2, kernel.size().width / 2), 1);
//                Mat closing = new Mat(opening.size(), opening.type());
//                Mat closing1 = new Mat();
//                kernel = getStructuringElement(MORPH_ELLIPSE, new Size(16, 16));
//                morphologyEx(opening, closing, MORPH_CLOSE, kernel, new Point(kernel.size().height / 2, kernel.size().width / 2), 3);

//                Mat closing1 = new Mat();
//                pyrUp(closing, closing1);
//                Mat distance = new Mat(binaryMap.size(), binaryMap.type());
//                distanceTransform(opening, distance, DIST_L2, 5, CV_8UC1);
//                Mat distance1 = new Mat();
//                distance.convertTo(distance1, CV_8UC1);
//                Mat foreground = new Mat(binaryMap.size(), binaryMap.type());
//                threshold(distance1, foreground, 0, 255, THRESH_BINARY | THRESH_OTSU);

//                List<Mat> subMats = new ArrayList<>();
//                List<Rect> subRects = new ArrayList<>();
//                for (int i = 0;i < 4;i++) {
//                    for (int j = 0;j <4;j++) {
//                        subMats.add(new Mat(binaryMap, new Rect(i * 25, j * 25, 25, 25)));
//                        subRects.add(new Rect(i * 25, j * 25, 25, 25));
//                    }
//                }
//                Double maxSum = 0.0;
//                int maxIndex = 0;
//                for (int i = 0;i < subMats.size();i++) {
//                    Double sum = sumElems(subMats.get(i)).val[0];
//                    if (sum > maxSum) {
//                        maxSum = sum;
//                        maxIndex = i;
//                    }
//                }

                /////////////////////////////
                // FACE DETECTOR
                /////////////////////////////
                mAbsoluteFaceSize = 0;

                Mat mGray = new Mat();
                cvtColor(in, mGray, COLOR_BGR2GRAY);

                if (mAbsoluteFaceSize == 0) {
                    int height = mGray.rows();
                    if (Math.round(height * mRelativeFaceSize) > 0) {
                        mAbsoluteFaceSize = Math.round(height * mRelativeFaceSize);
                    }
                }

                MatOfRect faces = new MatOfRect();
                if (mJavaDetector != null)
                    mJavaDetector.detectMultiScale(mGray, faces, 1.1, 3, 2, // TODO: objdetect.CV_HAAR_SCALE_IMAGE
                            new Size(mAbsoluteFaceSize, mAbsoluteFaceSize), new Size());

                Rect[] facesArray = faces.toArray();
                for (int i = 0; i < facesArray.length; i++)
                    Imgproc.rectangle(closing, facesArray[i].tl(), facesArray[i].br(), FACE_RECT_COLOR, -1);

                Rect thumbnailRect = new Rect();

                if (cropSizeValues[currentCropSize] == CROP_SIZE_ADAPTIVE) {

                    /////////////////////////////
                    // CONTOUR
                    /////////////////////////////

                    List<MatOfPoint> contours = new ArrayList<>();
                    Mat heirarchy = new Mat();
                    findContours(closing, contours, heirarchy, RETR_LIST, CHAIN_APPROX_SIMPLE);
                    sortContoursByArea(contours);

                    closing.convertTo(closing1, CV_8UC4);

                    if (contours.size() != 0) {
                        thumbnailRect = boundingRect(contours.get(0));
                        cvtColor(closing1, closing1, COLOR_GRAY2BGRA);
                        Imgproc.rectangle(closing1, thumbnailRect.tl(), thumbnailRect.br(), new Scalar(0, 255, 0, 64), 3);
                    }
                } else {

                    /////////////////////////////
                    // SLIDING WINDOW
                    /////////////////////////////

//                    List<Mat> subMats = new ArrayList<>();
//                    List<Rect> subRects = new ArrayList<>();
//                    for (int i = 0; i < 4; i++) {
//                        for (int j = 0; j < 4; j++) {
//                            subMats.add(new Mat(binaryMap, new Rect(i * 25, j * 25, 25, 25)));
//                            subRects.add(new Rect(i * 25, j * 25, 25, 25));
//                        }
//                    }

                    thumbnailRect = new Rect(0, 0, cropSizeValue, cropSizeValue);

                    double maxSum = 0.0;
                    //Rect maxRect = new Rect(0, 0, cropSizeValue, cropSizeValue);

                    for (int row = 0; row <= closing.rows() - cropSizeValue; row += slidingStep) {

                        for (int col = 0; col <= closing.cols() - cropSizeValue; col += slidingStep) {

                            Rect windows = new Rect(col, row, cropSizeValue, cropSizeValue);
                            Mat Roi = new Mat(closing, windows);
                            double sum = sumElems(Roi).val[0];
                            if (sum > maxSum) {
                                maxSum = sum;
                                thumbnailRect = windows;
                            }
                        }
                    }

//                    while (maxRect.br().y <= closing.height() - 1) {
//                        while (maxRect.br().x <= closing.width() - 1) {
//                            Mat maxMat = new Mat(closing, maxRect);
//                            Double tmp = sumElems(maxMat).val[0];
//                            if (tmp > maxSum) {
//                                maxSum = tmp;
//                                thumbnailRect = maxRect;
//                            }
//                            maxRect.x += slidingStep;
//                        }
//                        maxRect.y += slidingStep;
//                        maxRect.x = 0;
//                    }

                    closing.convertTo(closing1, CV_8UC4);
                    cvtColor(closing1, closing1, COLOR_GRAY2BGRA);
                    Imgproc.rectangle(closing1, thumbnailRect.tl(), thumbnailRect.br(), new Scalar(0, 255, 0, 64), 24);

//                    Double maxSum = 0.0;
//                    int maxIndex = 0;
//                    for (int i = 0; i < subMats.size(); i++) {
//                        Double sum = sumElems(subMats.get(i)).val[0];
//                        if (sum > maxSum) {
//                            maxSum = sum;
//                            maxIndex = i;
//                        }
//                    }
                }

                /////////////////////////////
                // OUTPUT
                /////////////////////////////

                afterProcessBit = Bitmap.createBitmap(blur.cols(), blur.rows(), Bitmap.Config.ARGB_8888);
                Utils.matToBitmap(blur, afterProcessBit);
                afterProcessImg.setImageBitmap(afterProcessBit);

                binaryBit = Bitmap.createBitmap(closing1.cols(), closing1.rows(), Bitmap.Config.ARGB_8888);
                Utils.matToBitmap(closing1, binaryBit);
                binaryImg.setImageBitmap(binaryBit);


                thumbnailBit = Bitmap.createBitmap(thumbnailRect.width, thumbnailRect.height, Bitmap.Config.ARGB_8888);
                Utils.matToBitmap(new Mat(in, thumbnailRect), thumbnailBit);
                thumbnailImg.setImageBitmap(thumbnailBit);

//                thumbnailBit = Bitmap.createBitmap(25, 25, Bitmap.Config.ARGB_8888);
//                Utils.matToBitmap(new Mat(in, subRects.get(maxIndex)), thumbnailBit);
//                thumbnailImg.setImageBitmap(thumbnailBit);
            }
        };
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.main_menu_settings:
                Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
                startActivity(intent);
                return true;
            case R.id.main_menu_help:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        if (reqCode == GALLERY_REQUEST) {
            if (resultCode == RESULT_OK) {
                try {
                    final Uri imageUri = data.getData();
                    String name = imageUri.getPath();
                    int cut = name.lastIndexOf('/');
                    if (cut != -1) {
                        name = name.substring(cut + 1);
                    }
                    fileName = name;
//                    final InputStream imageStream = getContentResolver().openInputStream(imageUri);
//                    BitmapFactory.Options options = new BitmapFactory.Options();
//                    options.inJustDecodeBounds = true;
//                    final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                    beforeProcessBit = decodeUri(getApplicationContext(), imageUri, 256, 256);
                    beforeProcessImg.setImageBitmap(beforeProcessBit);
                    evaluateCropSize();
                    processImage().onClick(loadBtn);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                }

            } else {
                Toast.makeText(MainActivity.this, "Вы не выбрали изображение", Toast.LENGTH_LONG).show();
            }
        }

    }

    public void evaluateCropSize() {
        cropSizeValue = cropSizeValues[currentCropSize] / scaleCoeff;
    }

    public Bitmap decodeUri(Context c, Uri uri, final int requiredWidth, final int requiredHeight)
            throws FileNotFoundException {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(c.getContentResolver().openInputStream(uri), null, o);

        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;

        while (true) {
            if (width_tmp / 2 < requiredWidth || height_tmp / 2 < requiredHeight)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        scaleCoeff = scale;
        return BitmapFactory.decodeStream(c.getContentResolver().openInputStream(uri), null, o2);
    }

    void configureBackdrop() {
        BottomSheetBehavior bsb = BottomSheetBehavior.from(findViewById(R.id.process_steps));
        bsb.setState(BottomSheetBehavior.STATE_COLLAPSED);

        mBottomSheetBehavior = bsb;

        showBackdrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                    showBackdrop.setText("Меньше");
                } else {
                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    showBackdrop.setText("Больше");
                }
            }
        });

        saveSaliencyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveImageMethod(getApplicationContext(), afterProcessBit, "Saliency");
            }
        });

        saveBinaryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveImageMethod(getApplicationContext(), binaryBit, fileName);
//                saveImageMethod(getApplicationContext(), binaryBit, "Binary");
            }
        });

        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        showBackdrop.setText("Больше");
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        showBackdrop.setText("Меньше");
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View view, float v) {

            }
        });
    }
}
