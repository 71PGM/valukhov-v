package ru.rummy.cropper.repos;

import android.graphics.Bitmap;

import org.opencv.core.Mat;

public interface IMainRepo {
    Bitmap loadImage();

    Mat decodeImage(Bitmap image);

    Bitmap encodeImage(Mat image);

    void saveImage(Bitmap image);
}
