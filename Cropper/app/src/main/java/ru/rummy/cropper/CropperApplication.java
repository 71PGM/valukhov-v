package ru.rummy.cropper;

import android.app.Application;
import android.util.Log;

import org.opencv.android.OpenCVLoader;

public class CropperApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        if (!OpenCVLoader.initDebug())
            Log.d("ERROR", "Unable to load OpenCV");
        else
            Log.d("SUCCESS", "OpenCV loaded");
    }
}
