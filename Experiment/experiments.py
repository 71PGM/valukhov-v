import cv2
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import tkinter
import tkinter.filedialog

from matplotlib.backends.backend_tkagg import (FigureCanvasTkAgg, NavigationToolbar2Tk)
# Implement the default Matplotlib key bindings.
from matplotlib.backend_bases import key_press_handler
from matplotlib.figure import Figure

if __name__ == '__main__':
	root = tkinter.Tk()
	root.wm_title("Experiment")
	
	fig = Figure()
	fnFixations = ""
	fnSaliency = ""
	
	NEED_SIZE = 176
	
	def _largestContoursRect(saliency):
		contours, hierarchy = cv2.findContours(saliency * 1,cv2.RETR_LIST,cv2.CHAIN_APPROX_SIMPLE)
		contours = sorted(contours, key = cv2.contourArea)
		#x, y, w, h = cv2.boundingRect(contours[-1])
		return cv2.boundingRect(contours[-1])
	
	def _intersection(a,b):
		x = max(a[0], b[0])
		y = max(a[1], b[1])
		w = min(a[0]+a[2], b[0]+b[2]) - x
		h = min(a[1]+a[3], b[1]+b[3]) - y
		if w<0 or h<0: return (0,0,0,0) # or (0,0,0,0) ?
		return (x, y, w, h)
	
	def _evaluateDiff():
		print("\nEvaluation")
		global fnFixations
		global fnSaliency
		try:
			imgFixations = cv2.imread(fnFixations)
			imgFixations = cv2.cvtColor(imgFixations, cv2.COLOR_BGR2GRAY)
			imgSaliency = cv2.imread(fnSaliency)
			imgSaliency = cv2.cvtColor(imgSaliency, cv2.COLOR_BGR2GRAY)
			
			pl1 = fig.add_subplot(2,2,1)
			pl1.imshow(cv2.cvtColor(imgFixations, cv2.COLOR_BGR2RGB))
			
			pl2 = fig.add_subplot(2,2,2)
			pl2.imshow(cv2.cvtColor(imgSaliency, cv2.COLOR_BGR2RGB))
			
			imgFixations = cv2.resize(imgFixations, (imgSaliency.shape[1], imgSaliency.shape[0]))
			imgFixations = cv2.threshold(imgFixations.astype("uint8"), 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
			imgSaliency = cv2.threshold(imgSaliency.astype("uint8"), 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
			
			x1, y1, w1, h1 = _largestContoursRect(imgFixations)
			imgFixations = cv2.cvtColor(imgFixations, cv2.COLOR_GRAY2BGR)
			cv2.rectangle(imgFixations, (x1, y1), (x1 + w1, y1 + h1), (0, 255, 0), 3)
			
			x2, y2, w2, h2 = _largestContoursRect(imgSaliency)
			imgSaliency = cv2.cvtColor(imgSaliency, cv2.COLOR_GRAY2BGR)
			cv2.rectangle(imgSaliency, (x2, y2), (x2 + w2, y2 + h2), (0, 255, 0), 3)
			
			x3, y3, w3, h3 = _intersection((x1, y1, w1, h1), (x2, y2, w2, h2))
			cv2.rectangle(imgFixations, (x3, y3), (x3 + w3, y3 + h3), (0, 0, 255), 3)
			cv2.rectangle(imgSaliency, (x3, y3), (x3 + w3, y3 + h3), (0, 0, 255), 3)
			
			pl3 = fig.add_subplot(2,2,3)
			pl3.imshow(cv2.cvtColor(imgFixations, cv2.COLOR_BGR2RGB))
			
			pl4 = fig.add_subplot(2,2,4)
			pl4.imshow(cv2.cvtColor(imgSaliency, cv2.COLOR_BGR2RGB))
			
			print("Result = {0} %".format((w3 * h3)/(w1 * h1) * 100))
		finally:
			canvas.draw()
			print("\nfinished: Evaluation")
	
	# привязка фигуры к корню (окну)
	canvas = FigureCanvasTkAgg(fig, master=root)  # A tk.DrawingArea.
	canvas.draw()
	canvas.get_tk_widget().pack(side=tkinter.TOP, fill=tkinter.BOTH, expand=1)
	
	# добавление панели инструментов для холста 
	toolbar = NavigationToolbar2Tk(canvas, root)
	toolbar.update()
	canvas.get_tk_widget().pack(side=tkinter.TOP, fill=tkinter.BOTH, expand=1)
	
	def _quit():
		root.quit()     # stops mainloop
		root.destroy()  # this is necessary on Windows to prevent
					# Fatal Python Error: PyEval_RestoreThread: NULL tstate

	# добавить кнопку выхода
	button = tkinter.Button(master=root, text="Quit", command=_quit)
	button.pack(side=tkinter.BOTTOM)
	
	def _loadSaliency():
		print("\nLoad Saliency")
		global fnSaliency
		fnSaliency = tkinter.filedialog.Open(root, filetypes = [('*.png files', '.png'), ('*.jpg files', '.jpg')]).show()
		print("\nsuccess: Load Saliency")
	
	def _loadFixations():
		print("\nLoad Fixations")
		global fnFixations
		fnFixations = tkinter.filedialog.Open(root, filetypes = [('*.png files', '.png'), ('*.jpg files', '.jpg')]).show()
		print("\nsuccess: Load Fixations")
	
	buttonEvaluate = tkinter.Button(master=root, text="Evaluate Diff", command=_evaluateDiff)
	buttonEvaluate.pack(side=tkinter.BOTTOM)
	
	buttonSaliency = tkinter.Button(master=root, text="Load Saliency", command=_loadSaliency)
	buttonSaliency.pack(side=tkinter.BOTTOM)
	
	# добавить кнопку загрузки изображения и начала вычислений
	buttonFixations = tkinter.Button(master=root, text="Load Fixations", command=_loadFixations)
	buttonFixations.pack(side=tkinter.BOTTOM)

	tkinter.mainloop()
