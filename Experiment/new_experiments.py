import cv2
import numpy as np
import os
import argparse
import csv



if __name__ == '__main__':
	fnFixations = ""
	fnBinary = ""
	
	NEED_SIZE = 176
	
	def _largestContoursRect(saliency):
		contours, hierarchy = cv2.findContours(saliency * 1,cv2.RETR_LIST,cv2.CHAIN_APPROX_SIMPLE)
		contours = sorted(contours, key = cv2.contourArea)
		#x, y, w, h = cv2.boundingRect(contours[-1])
		return cv2.boundingRect(contours[-1])
	
	def _intersection(a,b):
		x = max(a[0], b[0])
		y = max(a[1], b[1])
		w = min(a[0]+a[2], b[0]+b[2]) - x
		h = min(a[1]+a[3], b[1]+b[3]) - y
		if w<0 or h<0: return (0,0,0,0) # or (0,0,0,0) ?
		return (x, y, w, h)
	
	def _evaluateDiff():
		print("Evaluation")
		global fnFixations
		global fnBinary
		try:
			imgFixations = cv2.imread(fnFixations)
			imgFixations = cv2.cvtColor(imgFixations, cv2.COLOR_BGR2GRAY)
			imgSaliency = cv2.imread(fnBinary)
			imgSaliency = cv2.cvtColor(imgSaliency, cv2.COLOR_BGR2GRAY)

			
			imgFixations = cv2.resize(imgFixations, (imgSaliency.shape[1], imgSaliency.shape[0]))
			imgFixations = cv2.threshold(imgFixations.astype("uint8"), 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
			imgSaliency = cv2.threshold(imgSaliency.astype("uint8"), 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
			
			x1, y1, w1, h1 = _largestContoursRect(imgFixations)
			imgFixations = cv2.cvtColor(imgFixations, cv2.COLOR_GRAY2BGR)
			cv2.rectangle(imgFixations, (x1, y1), (x1 + w1, y1 + h1), (0, 255, 0), 3)
			
			x2, y2, w2, h2 = _largestContoursRect(imgSaliency)
			imgSaliency = cv2.cvtColor(imgSaliency, cv2.COLOR_GRAY2BGR)
			cv2.rectangle(imgSaliency, (x2, y2), (x2 + w2, y2 + h2), (0, 255, 0), 3)
			
			x3, y3, w3, h3 = _intersection((x1, y1, w1, h1), (x2, y2, w2, h2))
			cv2.rectangle(imgFixations, (x3, y3), (x3 + w3, y3 + h3), (0, 0, 255), 3)
			cv2.rectangle(imgSaliency, (x3, y3), (x3 + w3, y3 + h3), (0, 0, 255), 3)
			

			print("{0}".format(fnFixations))
			print("Result = {0} %".format((w3 * h3)/(w1 * h1) * 100))
			global spamwriter
			spamwriter.writerow(["{0}".format(fnFixations), "{0}".format((w3 * h3)/(w1 * h1) * 100)])
		finally:
			print("----------------------")
	
	# construct the argument parser and parse the arguments
	ap = argparse.ArgumentParser()
	ap.add_argument("-f", "--dirfix", required=True, help="dir for fixations")
	ap.add_argument("-b", "--dirbin", required=True, help="dir for binary map")
	args = vars(ap.parse_args())
	
	dirFixations = args["dirfix"]
	dirBinary = args["dirbin"]
	
	listFilesFixations = [f for f in os.listdir(dirFixations)]
	listFilesBinary = [f for f in os.listdir(dirBinary)]
	
	csvFile = open('results.csv', 'w', newline = '')
	spamwriter = csv.writer(csvFile, delimiter=',')
	spamwriter.writerow(['Файл', 'Покрытие (%)'])
	
	for f in listFilesFixations:
		for ff in listFilesBinary:
			ind = ff.find(f)
			if ((ind == 0)):
				fnFixations = os.path.join(dirFixations, f)
				fnBinary = os.path.join(dirBinary, ff)
				_evaluateDiff()
				break
			else:
				continue
