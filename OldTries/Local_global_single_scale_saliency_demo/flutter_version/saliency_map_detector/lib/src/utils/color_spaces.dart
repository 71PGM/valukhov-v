import 'dart:math' as m;

///Standard-RGB to XYZ

Object fromSRGBtoXYZ({sR = 0, sG = 0, sB = 0}) {
  //sR, sG and sB (Standard RGB) input range = 0 ÷ 255
  //X, Y and Z output refer to a D65/2° standard illuminant.
  var var_R = (sR / 255);
  var var_G = (sG / 255);
  var var_B = (sB / 255);

  if (var_R > 0.04045)
    var_R = m.pow((var_R + 0.055) / 1.055, 2.4);
  else
    var_R = var_R / 12.92;
  if (var_G > 0.04045)
    var_G = m.pow((var_G + 0.055) / 1.055, 2.4);
  else
    var_G = var_G / 12.92;
  if (var_B > 0.04045)
    var_B = m.pow((var_B + 0.055) / 1.055, 2.4);
  else
    var_B = var_B / 12.92;

  var_R = var_R * 100;
  var_G = var_G * 100;
  var_B = var_B * 100;

  final X = var_R * 0.4124 + var_G * 0.3576 + var_B * 0.1805;
  final Y = var_R * 0.2126 + var_G * 0.7152 + var_B * 0.0722;
  final Z = var_R * 0.0193 + var_G * 0.1192 + var_B * 0.9505;

  return {'X': X, 'Y': Y, 'Z': Z};
}

class XYZ {
  final double X;
  final double Y;
  final double Z;

  XYZ({this.X, this.Y, this.Z});

  
}
