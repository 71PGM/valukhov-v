import cv2
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from lib import pySaliencyMap, jacobgilSaliency
import tkinter
import tkinter.filedialog

from matplotlib.backends.backend_tkagg import (FigureCanvasTkAgg, NavigationToolbar2Tk)
# Implement the default Matplotlib key bindings.
from matplotlib.backend_bases import key_press_handler
from matplotlib.figure import Figure

if __name__ == '__main__':
	root = tkinter.Tk()
	root.wm_title("Experiment")
	
	fig = Figure()
	fn = ""
	
	"""
	Получение карты салиентности модулем pySaliencyMap.py
	
	Детектор карты салиентности основан на работе
	L. Itti, C. Koch, E. Niebur, 
	A Model of Saliency-Based Visual Attention for Rapid Scene Analysis, 
	IEEE Transactions on Pattern Analysis and Machine Intelligence, Vol. 20, No. 11, pp. 1254-1259, Nov 1998.
	
	Бинаризация - обычный thresholding из OpenCV
	
	Салиентный регион извлекается grabCut из OpenCV
	В качестве маски используется бинаризованная карта салиентности
	"""
	def _compute():
		global fn
		fn = tkinter.filedialog.Open(root, filetypes = [('*.png files', '.png'), ('*.jpg files', '.jpg')]).show()
		img = cv2.imread(fn)
		try:
			pl1 = fig.add_subplot(4,4,1)
			pl1.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
			pl1.set_title('Input image')
			canvas.draw()
			imgsize = img.shape
			img_width  = imgsize[1]
			img_height = imgsize[0]
			sm = pySaliencyMap.pySaliencyMap(img_width, img_height)
			saliency_map = sm.SMGetSM(img)
			pl2 = fig.add_subplot(4,4,5)
			pl2.imshow(saliency_map, 'gray')
			pl2.set_title('Saliency map')
			canvas.draw()
			binarized_map = sm.SMGetBinarizedSM(img)
			pl3 = fig.add_subplot(4,4,9)
			pl3.imshow(binarized_map, 'gray')
			pl3.set_title('Binarilized saliency map')
			canvas.draw()
			salient_region = sm.SMGetSalientRegion(img)
			#mask = jacobgilSaliency.refine_saliency_with_grabcut(img, binarized_map)
			#salient_region = img*mask[:,:,np.newaxis]
			pl4 = fig.add_subplot(4,4,13)
			pl4.imshow(cv2.cvtColor(salient_region, cv2.COLOR_BGR2RGB))
			pl4.set_title('Salient region')
			print("first finished")
		finally:
			canvas.draw()
			compute2()

	"""
	Получение карты салиентности средствами OpenCV
	
	Детектор карты салиентности использует StaticSaliencySpectralResidual
	(на основе частотно-фазовых характеристик изображения)
	Из документации OpenCV
	Starting from the principle of natural image statistics,
	this method simulate the behavior of pre-attentive visual search.
	The algorithm analyze the log spectrum of each image and obtain the spectral residual.
	Then transform the spectral residual to spatial domain to obtain the saliency map,
	which suggests the positions of proto-objects.
	
	Бинаризация - обычный thresholding из OpenCV
	
	Салиентный регион извлекается grabCut из OpenCV
	В качестве маски используется бинаризованная карта салиентности
	"""
	def compute2():
		img = cv2.imread(fn)
		try:
			pl1 = fig.add_subplot(4,4,2)
			pl1.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
			pl1.set_title('Input image')
			canvas.draw()
			saliency = cv2.saliency.StaticSaliencySpectralResidual_create()
			# saliencyMap - матрица, значения которой в интервале [0,1]
			(success, saliencyMap) = saliency.computeSaliency(img)
			# масштабирование карты (приведение в grayscale)
			saliencyMap = (saliencyMap * 255).astype("uint8")
			pl5 = fig.add_subplot(4,4,6)
			pl5.imshow(saliencyMap, 'gray')
			pl5.set_title('Saliency map')
			canvas.draw()
			# cv2.THRESH_OTSU - метод Оцу - выбор оптимального порога
			# Из документации
			# First argument is the source image, which should be a grayscale image.
			# Second argument is the threshold value which is used to classify the pixel values.
			# Third argument is the maxVal which represents the value to be given if pixel value is more than (sometimes less than) the threshold value.
			# OpenCV provides different styles of thresholding and it is decided by the fourth parameter of the function.
			threshMap = cv2.threshold(saliencyMap.astype("uint8"), 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
			pl6 = fig.add_subplot(4,4,10)
			pl6.imshow(threshMap, 'gray')
			pl6.set_title('Binarilized saliency map')
			canvas.draw()
			#mask = jacobgilSaliency.refine_saliency_with_grabcut(img, threshMap)
			#salientRegion = img*mask[:,:,np.newaxis]
			salientRegion = SMGetSalientRegion(img, threshMap)
			pl7 = fig.add_subplot(4,4,14)
			pl7.imshow(cv2.cvtColor(salientRegion, cv2.COLOR_BGR2RGB))
			pl7.set_title('Salient region')
			print("second finished")
		finally:
			canvas.draw()
			compute3()
	
	"""
	Получение карты салиентности средствами OpenCV
	
	Детектор карты салиентности использует StaticSaliencyFineGrained
	Из документации OpenCV
	This method calculates saliency based on center-surround differences.
	High resolution saliency maps are generated in real time by using integral images.
	
	Бинаризация - обычный thresholding из OpenCV
	
	Салиентный регион извлекается grabCut из OpenCV
	В качестве маски используется бинаризованная карта салиентности
	"""
	def compute3():
		img = cv2.imread(fn)
		try:
			pl1 = fig.add_subplot(4,4,3)
			pl1.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
			pl1.set_title('Input image')
			canvas.draw()
			saliency = cv2.saliency.StaticSaliencyFineGrained_create()
			# saliencyMap - матрица, значения которой в интервале [0,1]
			(success, saliencyMap) = saliency.computeSaliency(img)
			# масштабирование карты (приведение в grayscale)
			saliencyMap = (saliencyMap * 255).astype("uint8")
			pl5 = fig.add_subplot(4,4,7)
			pl5.imshow(saliencyMap, 'gray')
			pl5.set_title('Saliency map')
			canvas.draw()
			# cv2.THRESH_OTSU - метод Оцу - выбор оптимального порога
			# Из документации OpenCV
			# First argument is the source image, which should be a grayscale image.
			# Second argument is the threshold value which is used to classify the pixel values.
			# Third argument is the maxVal which represents the value to be given if pixel value is more than (sometimes less than) the threshold value.
			# OpenCV provides different styles of thresholding and it is decided by the fourth parameter of the function.
			threshMap = cv2.threshold(saliencyMap.astype("uint8"), 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
			pl6 = fig.add_subplot(4,4,11)
			pl6.imshow(threshMap, 'gray')
			pl6.set_title('Binarilized saliency map')
			canvas.draw()
			#mask = jacobgilSaliency.refine_saliency_with_grabcut(img, threshMap)
			#salientRegion = img*mask[:,:,np.newaxis]
			salientRegion = SMGetSalientRegion(img, threshMap)
			pl7 = fig.add_subplot(4,4,15)
			pl7.imshow(cv2.cvtColor(salientRegion, cv2.COLOR_BGR2RGB))
			pl7.set_title('Salient region')
			print("third finished")
		finally:
			canvas.draw()
			compute4()
	
	"""
	Получение карты салиентности модулем jacobgilSaliency.py
	
	Детектор карты салиентности использует авторский подход
	Используются гистограммы HSV
	Затем определяется функция плотности вероятности принадлежности к фону (backprojection)
	Карта салиентности - это инвертированная backprojection
	
	Бинаризация - обычный thresholding из OpenCV
	
	Салиентный регион извлекается grabCut из OpenCV
	В качестве маски используется кусок бинаризованной карты выпуклости
	Этот кусок определяется поиском самого большого контура
	"""	
	def compute4():
		img = cv2.imread(fn)
		try:
			pl1 = fig.add_subplot(4,4,4)
			pl1.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
			pl1.set_title('Input image')
			canvas.draw()
			saliencyMap = jacobgilSaliency.saliency_by_backprojection(img)
			pl8 = fig.add_subplot(4,4,8)
			pl8.imshow(saliencyMap, 'gray')
			pl8.set_title('Saliency map')
			canvas.draw()
			threshMap = jacobgilSaliency.saliency_map(img)
			pl9 = fig.add_subplot(4,4,12)
			pl9.imshow(threshMap, 'gray')
			pl9.set_title('Binarilized saliency map')
			canvas.draw()
			# поиск самого большого контура и формирование маски
			mask = jacobgilSaliency.refine_saliency_with_grabcut(img, threshMap)
			
			# обрезка изображения (умножение матриц маски и изображения)
			# скопировано из модуля jacobgilSaliency.py
			salientRegion = img*mask[:,:,np.newaxis]
			
			# другой вариант обрезки
			#salientRegion = SMGetSalientRegion(img, threshMap)
			
			pl10 = fig.add_subplot(4,4,16)
			pl10.imshow(cv2.cvtColor(salientRegion, cv2.COLOR_BGR2RGB))
			pl10.set_title('Salient region')
			print("fourth finished")
		finally:
			canvas.draw()
			print("ALL FINISHED")
	
	"""
	Обрезка оригинального изображения
	Скопировано из модуля pySaliencyMap.py
	
	Из Википедии
	GrabCut is an image segmentation method based on graph cuts.
	Starting with a user-specified bounding box around the object to be segmented,
	the algorithm estimates the color distribution of the target object and that of the background using a Gaussian mixture model.
	
	Из документации OpenCV
	Runs the GrabCut algorithm.
	The function implements the GrabCut image segmentation algorithm.
	
	img			Input 8-bit 3-channel image.
	
	mask		Input/output 8-bit single-channel mask.
				The mask is initialized by the function when mode is set to GC_INIT_WITH_RECT.
				Its elements may have one of the cv::GrabCutClasses.
			
	rect		ROI containing a segmented object.
				The pixels outside of the ROI are marked as "obvious background".
				The parameter is only used when mode==GC_INIT_WITH_RECT.
			
	bgdModel	Temporary array for the background model.
				Do not modify it while you are processing the same image.
				
	fgdModel	Temporary arrays for the foreground model. 
				Do not modify it while you are processing the same image.
				
	iterCount	Number of iterations the algorithm should make before returning the result.
				Note that the result can be refined with further calls with mode==GC_INIT_WITH_MASK or mode==GC_EVAL .
				
	mode		Operation mode that could be one of the cv::GrabCutModes
	
	GC_BGD 	
	an obvious background pixels

	GC_FGD 	
	an obvious foreground (object) pixel

	GC_PR_BGD 	
	a possible background pixel

	GC_PR_FGD 	
	a possible foreground pixel
	
	GC_INIT_WITH_MASK 	
	The function initializes the state using the provided mask.
	Note that GC_INIT_WITH_RECT and GC_INIT_WITH_MASK can be combined.
	Then, all the pixels outside of the ROI are automatically initialized with GC_BGD .
	"""
	def SMGetSalientRegion(src, srcBin):
		# get a binarized saliency map
		binarized_SM = srcBin.copy()
		# GrabCut
		img = src.copy()
		mask =  np.where((binarized_SM!=0), cv2.GC_PR_FGD, cv2.GC_PR_BGD).astype('uint8')
		bgdmodel = np.zeros((1,65),np.float64)
		fgdmodel = np.zeros((1,65),np.float64)
		rect = (0,0,1,1)  # dummy
		iterCount = 1
		cv2.grabCut(img, mask=mask, rect=rect, bgdModel=bgdmodel, fgdModel=fgdmodel, iterCount=iterCount, mode=cv2.GC_INIT_WITH_MASK)
		# post-processing
		mask_out = np.where((mask==cv2.GC_FGD) + (mask==cv2.GC_PR_FGD), 255, 0).astype('uint8')
		output = cv2.bitwise_and(img,img,mask=mask_out)
		return output	
	
	# привязка фигуры к корню (окну)
	canvas = FigureCanvasTkAgg(fig, master=root)  # A tk.DrawingArea.
	canvas.draw()
	canvas.get_tk_widget().pack(side=tkinter.TOP, fill=tkinter.BOTH, expand=1)
	
	# добавление панели инструментов для холста 
	toolbar = NavigationToolbar2Tk(canvas, root)
	toolbar.update()
	canvas.get_tk_widget().pack(side=tkinter.TOP, fill=tkinter.BOTH, expand=1)
	
	"""
	def on_key_press(event):
		print("you pressed {}".format(event.key))
		key_press_handler(event, canvas, toolbar)


	canvas.mpl_connect("key_press_event", on_key_press)
	"""
	def _quit():
		root.quit()     # stops mainloop
		root.destroy()  # this is necessary on Windows to prevent
					# Fatal Python Error: PyEval_RestoreThread: NULL tstate

	# добавить кнопку выхода
	button = tkinter.Button(master=root, text="Quit", command=_quit)
	button.pack(side=tkinter.BOTTOM)
	
	def _loadImage():
		print("\nSTART")
		_compute()
	
	# добавить кнопку загрузки изображения и начала вычислений
	button = tkinter.Button(master=root, text="Load", command=_loadImage)
	button.pack(side=tkinter.BOTTOM)

	tkinter.mainloop()
# If you put root.destroy() here, it will cause an error if the window is
# closed with the window manager.

	#plt.show()
#    cv2.waitKey(0)
	#cv2.destroyAllWindows()
